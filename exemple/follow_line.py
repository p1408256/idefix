from .aseba_api import Robot

def debug(varname, value, message=""):
    print("======================================================")
    print("                     DEBUG")
    print("------------------------------------------------------")
    print(" varname : " + varname)
    print(" value :   " + value)
    print("------------------------------------------------------")
    print(message)
    print("======================================================")

def follow_line():

    robot = Robot()

    # get the values of the sensors
    robot.get_sensor_values("under_reflect")
    debug("robot.sensor_value", str(robot.sensor_value))


    """
        ^   Mon code   ^
        v copié collé v
    """


    ##print the proximity sensors value in the terminal
    # print proxSensorsVal[0],proxSensorsVal[1],proxSensorsVal[2],proxSensorsVal[3],proxSensorsVal[4]

    # Parameters of the Braitenberg, to give weight to each wheels
    leftWheel = [-0.01, -0.005, -0.0001, 0.006, 0.015]
    rightWheel = [0.012, +0.007, -0.0002, -0.0055, -0.011]

    # Braitenberg algorithm
    totalLeft = 0
    totalRight = 0
    for i in range(len(robot.sensor_value)):
        totalLeft = totalLeft + (robot.sensor_value[i] * leftWheel[i] * 10)
        totalRight = totalRight + (robot.sensor_value[i] * rightWheel[i] * 10)

    # add a constant speed to each wheels so the robot moves always forward
    totalRight = totalRight + 500
    totalLeft = totalLeft + 500

    ##print in terminal the values that is sent to each motor
    # print "totalLeft"
    # print totalLeft
    # print "totalRight"
    # print totalRight

    # send motor value to the robot
    network.SetVariable("thymio-II", "motor.left.target", [totalLeft])
    network.SetVariable("thymio-II", "motor.right.target", [totalRight])

    return True