from pdf2image import convert_from_path, convert_from_bytes
from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)
from PIL import Image

paths = [ "Line-codes.pdf", "Quad-codes.pdf", "Tri-codes.pdf" ]
images = []


for f in paths:
    images += convert_from_path( f )


widths, heights = zip(*(i.size for i in images))

total_width  = max(widths) * 2
total_height = int( sum(heights) // 1.9 )

print(total_width, total_height)

new_im = Image.new('RGBA', (total_width, total_height))

y_offset = 0
x_offset = 0
k = 0
for im in images:
    k += 1
    new_im.paste(im, (x_offset,y_offset))
    y_offset += im.size[1]
    if k == len(images)//2:
        x_offset += im.size[0]
        y_offset = 0


#1 pixel = 0.0264583333 cm

final_width  = int( 2* 12.5 / 0.0264583333 )
final_height = int( total_height * final_width / total_width )

new_im.resize((final_width, final_height), Image.ANTIALIAS)

new_im.save('test-code-image.png')

