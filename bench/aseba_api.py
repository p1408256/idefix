import dbus
import dbus.mainloop.glib
from gi.repository import GObject as gobject
from optparse import OptionParser
import time
import signal


class Robot:

    def __init__(self):
        parser = OptionParser()
        parser.add_option("-s", "--system", action="store_true", dest="system", default=False,
                          help="use the system bus instead of the session bus")

        (options, args) = parser.parse_args()
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

        if options.system:
            bus = dbus.SystemBus()
        else:
            bus = dbus.SessionBus()

        self.network = dbus.Interface(bus.get_object('ch.epfl.mobots.Aseba', '/'),
                                      dbus_interface='ch.epfl.mobots.AsebaNetwork')
        self.sensor_value = None
        self.button_state = None
        self.loop = None
        self.handle = None

        print(self.network.GetNodesList())

        # end handling
        signal.signal(signal.SIGINT, self.signal_handling)

        self.buttons = {
            "center": "button.center",
            "back": "button.backward",
            "front": "button.forward",
            "right": "button.right",
            "left": "button.left"
        }
        self.motors = {
            "right": "motor.right.target",
            "left": "motor.left.target"
        }
        self.sensors = {
            "horizontal": "prox.horizontal",
            "under_ambiant": "prox.ground.ambiant",
            "under_reflect": "prox.ground.reflected",
            "under_delta": "prox.ground.delta"
        }
        self.leds = {
            "top": "leds.top",
            "under_left": "leds.bottom.left",
            "under_right": "leds.bottom.right",
            "circle": "leds.circle"
        }
        self.T = "thymio-II"
        self.sensor_value = None

        print("============")
        print("| Robot ok |")
        print("============")

    def run(self, delay, function):
        self.loop = gobject.MainLoop()
        self.handle = gobject.timeout_add(delay, function)
        self.loop.run()

    def errorLoop(self, e):
        print(e)
        self.loop.quit()

    def signal_handling(self, signum, frame):
        self.loop.quit()

    def set_motor_value(self, value, side="both"):
        """
        :param value: motor's new value.
        :param side: motor's side in {both, left, right}
        """
        if side == "both":
            self.network.SetVariable(self.T, self.motors["right"], [value])
            self.network.SetVariable(self.T, self.motors["left"], [value])
        elif side in ["right", "left"]:
            self.network.SetVariable(self.T, self.motors[side], [value])

    def get_sensor_values_getter(self, X):
        self.sensor_value = X

    def get_sensor_values(self, side="horizontal"):
        """
        :param side: Sensor's side in {horizontal, under_ambiant, under_reflect, under_delta}
        :return: sensor value for sensor at "side"
        """
        sensor = self.sensors[side]
        self.network.GetVariable(self.T, sensor, reply_handler=self.get_sensor_values_getter,
                                 error_handler=self.errorLoop)
        return self.sensor_value

    def get_button_state_getter(self, x):
        self.button_state = x

    def get_button_state(self, side="center"):
        """
        :param side: button's side in {center, back, front, left, right}
        :return: Button state
        """
        button = self.buttons[side]
        self.network.GetVariable(self.T, button, reply_handler=self.get_button_state_getter,
                                 error_handler=self.errorLoop)
        return self.button_state

    def set_led_value(self, side="top", rgb_value=[0, 0, 0]):
        """ Change the color of a led.
        :param side: led's side in {top, under_left, under_right, circle}
        :param rgb_value: led's new color. if side = circle, value should be 8 leds colors [(0,0,0) ... (0,0,0)]
        """
        print(side, rgb_value)
        led = self.leds[side]
        self.network.SetVariable(self.T, led, rgb_value)
