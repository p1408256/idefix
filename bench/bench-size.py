#!/usr/bin/python3
from aseba_api import Robot
import matplotlib.pyplot as plt

robot = Robot()
memory = []

def Init():
    global speed
    speed = 115


def move_one_eye(eye=1):
    ''' folow line with one eye '''
    global speed

    under_sensor = robot.sensor_value
    if under_sensor != None:
        position = under_sensor[eye] - 525
        if abs(position) < 170:
            robot.set_motor_value(speed + position / 8)
        else:
            robot.set_motor_value(position / 4, "right" if eye == 1 else "left")
            robot.set_motor_value(-position / 4, "left" if eye == 1 else "right")

def read_code():
    global memory
    under_sensor = robot.sensor_value

    if under_sensor is not None:
        memory.append( under_sensor[0] )

def loop():
    under_sensor = robot.set_sensor_values("under_reflect")
    read_code()
    print(memory)
    move_one_eye(1)

    return True


Init()
robot.run(100, loop)

robot.set_motor_value(0)

plt.plot(memory, "-")
plt.savefig("size.pdf")
