from PIL import Image
import numpy as np
import os

# codes
# (.)   (┃ ━)   (╂ ╃ ╄ ╅ ╆ ┿) (┷ ┹ ┺ ┯ ┲ ┱ ┠ ┡ ┢ ┨ ┩ ┪) (┐ ┌ ┓ ┏ ┘ └ ┛ ┗)

# initialise codes

code_to_draw ={
            ".":["src/Empty-001.png",0,1000],
            " ":["src/Empty-001.png",0,1000],
            "┃":["src/Straight-001.png",90,1000],
            "━":["src/Straight-001.png",0,1000],

            "╂":["src/Quad-001.png",270,1000],
            "╃":["src/Quad-003.png",180,1000],
            "╄":["src/Quad-003.png",90,1000],
            "╅":["src/Quad-003.png",270,1000],
            "╆":["src/Quad-003.png",0,1000],
            "┿":["src/Quad-001.png",0,1000],

            "┷":["src/Tri-003.png",270,1000],
            "┹":["src/Tri-001.png",270,1000],
            "┺":["src/Tri-002.png",270,1000],
            "┯":["src/Tri-003.png",90,1000],
            "┲":["src/Tri-001.png",90,1000],
            "┱":["src/Tri-001.png",90,1000],
            "┠":["src/Tri-003.png",180,1000],
            "┡":["src/Tri-001.png",180,1000],
            "┢":["src/Tri-002.png",180,1000],
            "┨":["src/Tri-003.png",0,1000],
            "┩":["src/Tri-002.png",0,1000],
            "┪":["src/Tri-001.png",0,1000],

            "┐":["src/Circ-001.png",90,1000],
            "┌":["src/Circ-001.png",180,1000],
            "┘":["src/Circ-001.png",0,1000],
            "└":["src/Circ-001.png",270,1000],
    }

# get map char
path = "/".join(os.path.realpath(__file__).split('/')[:-1]) + "/map.txt"
f=open(path)
M = np.array([np.array(list(l[:-1])) for l in f if l[0]!="#"])
f.close()


sizeX = max( map( lambda x:sum( map( lambda y: code_to_draw[y][2],x)), M ) ) -200
sizeY = max( map( lambda x:sum( map( lambda y: code_to_draw[y][2],x)), M.T ) ) -200
new_img = Image.new('RGBA', (sizeX, sizeY), "#15061c")

sy = -200
for y,l in enumerate(M):
    sx = -200
    for x,t in enumerate(l):
        img = Image.open(code_to_draw[t][0]).rotate(code_to_draw[t][1])
        new_img.paste(img, (sx, sy), img)
        sx += code_to_draw[t][2]
    sy += code_to_draw[t][2]

new_img.save("map.png")

f = open("generate.playground", "w")
f.write('<!DOCTYPE aseba-playground>\n<aseba-playground>\n	<color name="wall" r="0.9" g="0.9" b="0.9" />\n	<color name="red" r="0.77" g="0.2" b="0.15" />\n	<color name="green" r="0" g="0." b="0.17" />\n	<color name="blue" r="0" g="0.38" b="0.61" />\n	<world w="'+str(int(sizeX//12.5))+'" h="'+str(int(sizeY//12.5))+'" color="wall" groundTexture="map.png"/>\n	<robot type="thymio2" x="15" y="30" port="33333" />\n</aseba-playground>')

f.close()



