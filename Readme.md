# IDEFIX

## Objectif

Programmer un robot Thymio II à l'aide d'une rasberry pi
pour qu'il se déplace sur une route en respectant les
autres usagers.


## Note

On devra sans aucun doute en avoir un dans le rendu, au moins
maintenant il est là même si ce n'est pas très pertinent actuellement.

L'api est terminée (plus ou moins) il ne reste plus qu'a coder les chemins et autres algo de deplacement.

ya des tests plutot pas mal dans `test_simulation.py`, ca permet de comprendre comment ca fonctionne

## Usage

Tout fonctionne dans `asebaplayground` a condition de suivre les instruction ci-dessous

avant utilisation d'un des scripts comme `follow.py` : il faut lancer `launchaseba.sh`


## Machins qu'il faut avoir

```
python3
python3-gi
python3-dbus
python3-optarse

aseba
	dont asebaplayground

du courage
```
