import statistics
from enum import Enum

white_range = [1023, 900]
light_grey_range = [900, 818]
dark_grey_range = [818, 409]
black_range = [409, 0]

#[872.1, 962, 714.3, 509.27777777777777]

class ColorType(Enum):
    WHITE = {
        "range": [1023, 870],
        "value": 0,
    }
    LIGHT_GREY = {
        "range": [870, 800],
        "value": 1,
    }
    DARK_GREY = {
        "range": [800, 620],
        "value": 2,
    }
    BLACK = {
        "range": [620, 0],
        "value": 3,
    }


class Color:
    def __init__(self, value=None, type=None):
        assert value is not None or type is not None
        self.range = None
        self.type = None
        self.value = None
        if value is not None:
            if isinstance(value, float):
                value = int(value)
            assert isinstance(value, int)

            for type in ColorType:
                if type.value["range"][0] >= value > type.value["range"][1]:
                    self.range = type.value["range"]
                    self.type = type
                    self.value = value
                    break
        else:
            assert isinstance(type, ColorType)
            self.range = type.value["range"]
            self.type = type
            self.value = statistics.mean(self.range)

        self.index = self.type.value["value"]
        self.mean = statistics.mean(self.range)
        self.name = self.type.name

    def __eq__(self, c2):
        return self.type == c2.type

    def __lt__(self, other):
        return self.type.value["value"] < other.type.value["value"]

    def __gt__(self, other):
        return self.type.value["value"] > other.type.value["value"]

    def __str__(self):
        return "(Color: " + str(self.type.name) + ")"

