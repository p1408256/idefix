from enum import Enum


class Direction(Enum):
    """
    Directions from the car's point of view.
    """
    SELF = 1
    STRAIGHT = 2
    RIGHT = 3
    LEFT = 4
