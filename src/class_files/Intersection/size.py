from enum import Enum


class Size(Enum):
    SHORT = 1
    MEDIUM = 2
    LARGE = 3