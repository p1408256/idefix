from class_files.Intersection.direction import Direction
from class_files.Intersection.size import Size
from class_files.Color.color import ColorType
from random import choice
from random import seed


seed(1)


#code -> color_code
code_2_color = {
    "┃": [ColorType.WHITE, ColorType.WHITE, ColorType.WHITE, ColorType.WHITE],

#-----------------------
# code : intersection + route principal en gras

    "╂": [ColorType.BLACK, ColorType.WHITE, ColorType.BLACK, ColorType.WHITE],
    "╃": [ColorType.BLACK, ColorType.WHITE, ColorType.DARK_GREY, ColorType.WHITE],
    "╄": [ColorType.BLACK, ColorType.WHITE, ColorType.LIGHT_GREY, ColorType.WHITE],
    "╅": [ColorType.BLACK, ColorType.DARK_GREY, ColorType.WHITE, ColorType.WHITE],
    "╆": [ColorType.BLACK, ColorType.LIGHT_GREY, ColorType.WHITE, ColorType.WHITE],
    "┿": [ColorType.BLACK, ColorType.BLACK, ColorType.WHITE, ColorType.WHITE],

#------------------------
# code : intersetion + route principale en gras

    "┯": [ColorType.LIGHT_GREY, ColorType.BLACK, ColorType.WHITE, ColorType.BLACK],
    "┲": [ColorType.LIGHT_GREY, ColorType.LIGHT_GREY, ColorType.WHITE, ColorType.LIGHT_GREY],
    "┱": [ColorType.LIGHT_GREY, ColorType.DARK_GREY, ColorType.WHITE, ColorType.WHITE],

    "┠": [ColorType.LIGHT_GREY, ColorType.WHITE, ColorType.BLACK, ColorType.WHITE],
    "┡": [ColorType.LIGHT_GREY, ColorType.WHITE, ColorType.LIGHT_GREY, ColorType.BLACK],
    "┢": [ColorType.LIGHT_GREY, ColorType.LIGHT_GREY, ColorType.WHITE, ColorType.DARK_GREY],

    "┨": [ColorType.LIGHT_GREY, ColorType.WHITE, ColorType.BLACK, ColorType.LIGHT_GREY],
    "┩": [ColorType.LIGHT_GREY, ColorType.WHITE, ColorType.DARK_GREY, ColorType.BLACK],
    "┪": [ColorType.LIGHT_GREY, ColorType.DARK_GREY, ColorType.WHITE, ColorType.BLACK],

#------------------------
# code : taille des virages

    "┐": [ColorType.DARK_GREY, ColorType.DARK_GREY, ColorType.LIGHT_GREY, ColorType.WHITE],
    "┌": [ColorType.DARK_GREY, ColorType.LIGHT_GREY, ColorType.LIGHT_GREY, ColorType.WHITE],
    "┒": [ColorType.DARK_GREY, ColorType.DARK_GREY, ColorType.DARK_GREY, ColorType.WHITE],
    "┎": [ColorType.DARK_GREY, ColorType.LIGHT_GREY, ColorType.DARK_GREY, ColorType.WHITE],
    "┓": [ColorType.DARK_GREY, ColorType.DARK_GREY, ColorType.WHITE, ColorType.WHITE],
    "┏": [ColorType.DARK_GREY, ColorType.LIGHT_GREY, ColorType.WHITE, ColorType.WHITE]
}

#code -> (priority, unavailable, size)
code_2_direction = {
    "┃": ([Direction.STRAIGHT, Direction.SELF], [Direction.LEFT, Direction.RIGHT], Size.MEDIUM),

    "╂": ([Direction.STRAIGHT, Direction.SELF], [], Size.MEDIUM),
    "╃": ([Direction.STRAIGHT, Direction.LEFT], [], Size.MEDIUM),
    "╄": ([Direction.STRAIGHT, Direction.RIGHT], [], Size.MEDIUM),
    "╅": ([Direction.SELF, Direction.LEFT], [], Size.MEDIUM),
    "╆": ([Direction.SELF, Direction.RIGHT], [], Size.MEDIUM),
    "┿": ([Direction.LEFT, Direction.RIGHT], [], Size.MEDIUM),

    "┯": ([Direction.LEFT, Direction.RIGHT], [Direction.STRAIGHT], Size.MEDIUM),
    "┲": ([Direction.SELF, Direction.RIGHT], [Direction.STRAIGHT], Size.MEDIUM),
    "┱": ([Direction.SELF, Direction.LEFT], [Direction.STRAIGHT], Size.MEDIUM),

    "┠": ([Direction.SELF, Direction.STRAIGHT], [Direction.LEFT], Size.MEDIUM),
    "┡": ([Direction.STRAIGHT, Direction.RIGHT], [Direction.LEFT], Size.MEDIUM),
    "┢": ([Direction.SELF, Direction.RIGHT], [Direction.LEFT], Size.MEDIUM),

    "┨": ([Direction.SELF, Direction.STRAIGHT], [Direction.RIGHT], Size.MEDIUM),
    "┩": ([Direction.STRAIGHT, Direction.LEFT], [Direction.RIGHT], Size.MEDIUM),
    "┪": ([Direction.SELF, Direction.LEFT], [Direction.RIGHT], Size.MEDIUM),

    "┐": ([Direction.SELF, Direction.LEFT], [Direction.STRAIGHT, Direction.RIGHT], Size.SHORT),
    "┌": ([Direction.SELF, Direction.RIGHT], [Direction.STRAIGHT, Direction.LEFT], Size.SHORT),
    "┒": ([Direction.SELF, Direction.LEFT], [Direction.STRAIGHT, Direction.RIGHT], Size.MEDIUM),
    "┎": ([Direction.SELF, Direction.RIGHT], [Direction.STRAIGHT, Direction.LEFT], Size.MEDIUM),
    "┓": ([Direction.SELF, Direction.LEFT], [Direction.STRAIGHT, Direction.RIGHT], Size.LARGE),
    "┏": ([Direction.SELF, Direction.RIGHT], [Direction.STRAIGHT, Direction.LEFT], Size.LARGE),
}


class Intersection:
    def __init__(self, code=None, priority_dir=[], unavailable_dir=[], size=Size.MEDIUM):
        """
        Initialise intersection
        :param code: If not None, ignore others parameters and build the Intersection using the code.
        :param priority_dir: Directions that are inside the priority way.
        :param unavailable_dir: directions that are unavailable from this intersection
        :param size: intersection's size.
        """

        self.priority_dir = None
        self.unavailable_dir = None
        if code is not None:
            assert isinstance(code, list) and len(code) == 4
            assert all(isinstance(color, ColorType) for color in code)

            for c in code_2_color:
                if code_2_color[c] == code:
                    self.priority_dir, self.unavailable_dir, self.size = code_2_direction[c]
                    break


            assert self.priority_dir is not None and self.unavailable_dir is not None
        else:
            assert isinstance(priority_dir, list) and 0 <= len(priority_dir) <= 4
            assert all(isinstance(direction, Direction) for direction in priority_dir)

            assert isinstance(unavailable_dir, list) and 0 <= len(unavailable_dir) <= 4
            assert all(isinstance(direction, Direction) for direction in unavailable_dir)

            assert isinstance(size, Size)

            self.size = size
            self.priority_dir = priority_dir
            self.unavailable_dir = unavailable_dir

        self.available_dir = []
        self.set_available_dir()

    def set_available_dir(self):
        assert self.available_dir is not None and isinstance(self.available_dir, list)
        for direction in Direction:
            if direction not in self.unavailable_dir and direction is not Direction.SELF:
                self.available_dir.append(direction)

    def get_random_available_direction(self):
        return choice(self.available_dir)

    def is_priority(self, direction):
        assert isinstance(direction, Direction)
        return direction in self.priority_dir

class Comportements:
    def __init__(self,intersection):
        """
        Initialise Comportement
        :param intersection: intersection in front of me
        """
        self.intersection = intersection

    def get(self):

        timer_stop = 3
        timer_medium = 10
        forward_before_turning = 5

        medium_speed = 1.8

        my_dir = self.intersection.get_random_available_direction()
        size = self.intersection.size

        if self.intersection.is_priority(my_dir):
            return []
        elif my_dir == Direction.RIGHT:
            # s'arrêter 
            values = [[{"motor":[0,0]},{"timer":timer_stop}]]
            if size == Size.SHORT:
                # avancer doucement
                values.append([{"motor":[1.5,1]}, {"timer":10}])
            elif size == Size.MEDIUM:
                values.append([{"motor":[medium_speed,1]}, {"timer":timer_medium}])
            else:
                values.append([{"motor":[2,1]}, {"timer":10}])
            return values

        elif my_dir == Direction.LEFT:
            values = [[{"motor":[0,0]},{"timer":timer_stop}]]

            # cas d'un tri
            if len(my_dir.unavailable_dir) != 0:
                # si je viens d'une route prioritaire, je passe directement
                if self.intersection.is_priority(Direction.SELF):
                    values.append([{"motor":[1,1]}, {"timer":forward_before_turning}])
                    values.append([{"motor":[1,medium_speed]}, {"timer":timer_medium}])
                # sinon, attendre que tout le monde sur la voie prio passe pour que je passe
                else:
                    values.append([{"motor":[0,0]}, {"timer":timer_stop}])
                    # if personne_sur_ma_route:
                        # values.append({"motor":[1,1]}, {"timer":forward_before_turning})
                        # values.append({"motor":[1,medium_speed]}, {"timer":timer_medium})

            # cas d'un quad
            else: 
                # si je viens d'une route prioritaire et que je me dirige vers
                # une route prioritaire, je passe directement
                if self.intersection.is_priority(Direction.SELF) and self.instersection.is_priority(my_dir) :
                    values.append([{"motor":[1,1]}, {"timer":forward_before_turning}])
                    values.append([{"motor":[1,medium_speed]}, {"timer":timer_medium}])

                # si je viens d'une route prioritaire et que je me dirige vers
                # une route non prioritaire, j'attends avant de m'avancer pour passer
                elif self.instersection.is_priority(my_dir) != True and self.intersection.is_priority(Direction.SELF) :
                    values.append([{"motor":[1,1]}, {"timer":forward_before_turning}])
                    values.append([{"motor":[0,0]}, {"timer":timer_stop}])
                    values.append([{"motor":[1,medium_speed]}, {"timer":timer_medium}])

                # si je viens d'une route non prioritaire et que je me dirige vers
                # une route prioritaire, j'attends avant d'avancer pour passer 
                elif self.intersection.is_priority(Direction.SELF) != True and self.instersection.is_priority(my_dir):
                    values.append([{"motor":[0,0]}, {"timer":timer_stop}])
                    values.append([{"motor":[1,1]}, {"timer":forward_before_turning}])
                    values.append([{"motor":[1,medium_speed]}, {"timer":timer_medium}])

                # sinon, attendre que tout le monde sur la voie prio passe pour que je passe
                else:
                    values.append([{"motor":[0,0]}, {"timer":timer_stop}])
                    values.append([{"motor":[1,1]}, {"timer":forward_before_turning}])
                    values.append([{"motor":[1,medium_speed]}, {"timer":timer_medium}])


        return values


