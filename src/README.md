# API aseba for python

## documentation Thymio

je vous laisse regarder [ici](http://wiki.thymio.org/fr:thymioapi)

## initialisation

`run_simulation.sh` pour lancer la simulation ou pour relancer le code si la simulation tourne deja

`stop_simulation.sh` pour kill la simulation

## setter

l'api permet, si elle s'initialise de set la vitesse des moteurs
```
robot = Robot()
robot.set_motor_value( 100, side="both" ) #side ∈ ["both", "left", "right"]
```

et quand j'aurais cmopris pourquoi, elle permetra de set les couleurs aussi
WARNING CA MARCHE PAS ENCORE CA
```
robot = Robot()
r,g = 0
b = 1
color = [r,g,b]
robot.set_led_value( side="top", value=color ) #side ∈ ["both", "under_left", "under_right", "circle"]

#if circle -> value=[color1, color2, ..., color8]
```

## getter

les getter sont en 2 etapes (car c'est asynchrone)
1. on demande la valeur
2. on lit la valeur

sensor
```
robot = Robot()
robot.get_sensor_values( side="horizontal" ) #side ∈ ["horizontal", "under_ambiant", "under_reflect", "under_delta"]
value = robot.sensor_value

#pour les "under_*" c'est sous la forme [gauche, droite] pour "horizontal" c'est les 8 capteurs [c1, c2, c3, ..., c8]
```

button
```
robot = Robot()
robot.get_button_state( side="center" ) #side ∈ ["center", "back", "front", "left", "right"]
value = robot.button_state
```
