if [ ! -f running ]
then
	echo "Launch all aseba modules"
	touch running

	# start simulation
	asebaplayground ../simulation/idefix-01.playground &
	sleep 4

	# connect to simulation
	asebamedulla "tcp:localhost;33333" -p 33334 &

	# run the code
	sleep 2
fi

echo "Launch simulation"
python3 run.py
