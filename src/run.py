#!/usr/bin/python3
from aseba_api import Robot
from class_files.Color.color import Color
from class_files.Color.color import ColorType
from class_files.Intersection.intersection import code_2_color
from settings import *
import statistics
# import matplotlib.pyplot as plt


robot = Robot()
memory = []
lasts_matched = ["│"]
road_joined = False
reading = False
read_time = -1

# une case 6ticks
# entre 2 cases 2ticks


def follow_line_with_eye(eye=1):
    """ Follow line with one eye """
    global memory

    under_sensor = robot.sensor_value
    if under_sensor is not None:
        position = under_sensor[eye] - 300 # on sert le rail de deplacement un maximum a gauche pour pouvoir lire les codes avec le second capteur
        if abs(position) < 75:
            if robot.reading_code_since == -1:
                robot.set_motor_value(robot_speed + position / 10)
            else:
                robot.set_motor_value(robot_reading_speed + position / 10)
        else:
            robot.set_motor_value(position / 5, "right" if eye == 1 else "left")
            robot.set_motor_value(-position / 5, "left" if eye == 1 else "right")
            # memory.pop()

def read_code():
    global memory
    under_sensor = robot.sensor_value

    if under_sensor is not None:
        color = Color(value=under_sensor[0])
        memory.append(color)


def match(observed_sequence, code_pattern):
    """
    Get the error value between two colors codes.
    :param observed_sequence: An observation sequence. Can be a list of Colors or a list of floats.
    :param code_pattern: A list of Color, the code pattern used for the comparison.
    :return: 
    """
    assert isinstance(code_pattern, list) and len(code_pattern) == 4
    assert [isinstance(color, Color) for color in code_pattern]

    assert isinstance(observed_sequence, list) and len(observed_sequence) == 4
    assert [isinstance(elt, Color) for elt in observed_sequence] \
           or [isinstance(elt, int) and 0 <= elt <= 1023 for elt in observed_sequence]

    error = 0
    for observed, code_elt in zip(observed_sequence, code_pattern):
        if isinstance(observed_sequence[0], Color):
            error += (observed.index - code_elt.value) ** 2
        else:  # observed is a float between 0 and 1023
            error += (observed - Color(type=code_elt).mean) ** 2
    return round(error, 3)


def clean_memory():
    global memory
    except_last = 2

    # on veut retirer les valeurs etranges pour les cas [blanc / gris clair]
    if len(memory) > except_last + 1:
        for i in range(1, len(memory) - except_last):
            if memory[i - 1] == memory[i + 1] == Color(type=ColorType.WHITE):
                memory[i] = Color(type=ColorType.WHITE)
            elif memory[i - 1] == memory[i + 1] == Color(type=ColorType.LIGHT_GREY):
                memory[i] = Color(type=ColorType.LIGHT_GREY)


road_center = 500
road_border_passed = False
def join_road():
    global road_joined, road_border_passed
    # Drive full straight
    robot.set_motor_value(robot_speed)

    if robot.sensor_value is not None:
        ground = robot.sensor_value[1]
        if road_border_passed == False and ground < 400:
            road_border_passed = True
            print("border passed")
        elif road_border_passed and not road_joined and abs(ground - 525) < 80:
            road_joined = True
            print("road joined")

def observe_memory():  # = renamed function understand_code()
    global lasts_matched
    """
    Observe what we have in our memory, and see if we can interpret it as a known code.
    If it's the case, use this code.
    :return: Boolean, True if a code is observed
    """
    # If the memory have enough information to observe a code
    if len(memory) >= code_length:
        # Build an observation_sequence sampling
        observation_sequence = memory[-code_length+2:]  # observation_sequence_length last elements in memory (on regarde pas le trigger donc on retire 2 valeurs ~)
        sample_length = code_length // 4  # 4 -> Code length
        samples = []
        for sample_index in range(4):
            start = sample_index * sample_length
            sub_sequence = observation_sequence[start : start + sample_length]
            samples.append(statistics.mean([color.value for color in sub_sequence]))

        print(samples)
        best_match = [None, None]
        for c in code_2_color:
            v = match(samples, code_2_color[c])
            if best_match[0] is None or v < best_match[1]:
                best_match = [c, v]

        maximum_error = 100000
        if best_match[0] != "┃" and best_match[1] < maximum_error:
            lasts_matched.append( best_match[0] )
            # print(
            #     [type.name for type in code_2_color[best_match[0]]],
            #     len(memory),
            #     best_match,
            #     samples
            # )
            return True
    return False

def robot_start_reading_a_code():
    """ True if the robot is not reading a code """
    global memory
    return len(memory)!=0 and memory[-1] != Color( type=ColorType.WHITE)

def follow_line():
    """
    This function make the robot follow the line normally while there is no code.
    From the time the robot detect something different than white, the function pass it into reading mode.
    When the robot is done reading the code, this function analyse robot's memory to know which code has been read.
    :return:
    """
    global lasts_matched
    read_code()
    if robot.reading_code_since == -1:
        if robot_start_reading_a_code():
            print("Start reading a code ... ", end="\r")
            robot.set_motor_value(robot_reading_speed)
            robot.reading_code_since = 0
            follow_line_with_eye(1)
        else:
            follow_line_with_eye(1)
    else:
        if robot.reading_code_since == code_length:
            print(" "*30,  end="\r")
            # Stop reading mode
            robot.reading_code_since = -1
            # Code Analysis
            clean_memory()
            if observe_memory():
                #something detected
                print("in front of :", lasts_matched[-1])
            robot.set_motor_value(robot_speed)
        elif robot.reading_code_since == code_length // 4 + 2:
            # If we're done reading the first square
            clean_memory()

            # Build an observation_sequence sampling
            observation_sequence = memory[-code_length // 4:]
            color = statistics.median([color.value for color in observation_sequence])

            if color == 0:
                # our first color is white, stop reading
                robot.reading_code_since = -1
                robot.set_motor_value(robot_speed)
                follow_line_with_eye(1)
            else:
                # Continue reading code
                robot.reading_code_since += 1
                follow_line_with_eye(1)

        else:
            # Continue reading with incrementing reading time.
            robot.reading_code_since += 1
            follow_line_with_eye(1)

road_joined = False

def loop():
    global road_joined, reading, read_time, robot_speed, memory
    robot.set_sensor_values("under_reflect")

    if not road_joined:
        join_road()
    else:
        follow_line()

    return True

robot.run(100, loop)

print(len(memory))
robot.set_motor_value(0)

# plt.plot(memory, "-")
# plt.show()
