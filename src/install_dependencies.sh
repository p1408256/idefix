INSTALLER="apt install"

if [[ "$( lsb_release -i | cut -c 17- )" == "Arch" ]]
then
	echo "arch user"
	yay -S python3 pip aseba
	sudo pip install dbus-python pygobject gobject optparse-pretty
fi

if [[ "$( lsb_release -i | cut -c 17- )" == "Ubuntu" ]]
then
	echo "arch user"
	sudo apt install python3 python3-gi python3-dbus python3-optarse aseba
fi

